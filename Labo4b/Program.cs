﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labo4b
{
    class Program
    {
        static void Main(string[] args)
        {
            byte a = 12; // Coversion implicite 1
            short b = a; 

            int c = 20; // Conversion implicite 2
            double d = c;


            double e = 123.9; //Conversion Explicite 1
            int f = (int)e;

            float g = 12; //Conversion Explicite 2
            byte h = (byte)g;


            string nb1 = "1234"; //Conversion de chaine 1
            int i_nb1 = int.Parse(nb1);

            string nb2 = "1234.00"; //Conversion de chaine 2
            double d_nb2 = double.Parse(nb2, System.Globalization.CultureInfo.InvariantCulture);
           
            //Danic
            byte danic = 69;
            float fortier = danic;
            Console.WriteLine(fortier);
            Console.ReadLine();

            //test commit with TortoiseGit
        }
    }
}
