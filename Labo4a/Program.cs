﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compagnie;

namespace Labo4a
{
    class Program
    {
        static void Main(string[] args)
        {
            Employe[] vecteur = new Employe[3];
            int i = 0;
            do
            {
                string nom, sexe, division;
                
                Console.Write("Nom: ");
                nom = Console.ReadLine();
                Console.Clear();
                Console.Write("Sexe(Male / Femelle: ");
                sexe = Console.ReadLine();
                Console.Clear();
                Console.Write("Division: ");
                division = Console.ReadLine();
                Console.Clear();

                vecteur[i] = new Employe(nom, sexe, division);
                i++;
            } while (i != 3);
            Console.WriteLine("Employe 1: Nom:{0} Sexe: {1} Division: {2}", vecteur[0].Nom, vecteur[0].Sexe, vecteur[0].Division);
            Console.WriteLine("Employe 2: Nom:{0} Sexe: {1} Division: {2}", vecteur[1].Nom, vecteur[1].Sexe, vecteur[1].Division);
            Console.WriteLine("Employe 3: Nom:{0} Sexe: {1} Division: {2}", vecteur[2].Nom, vecteur[2].Sexe, vecteur[2].Division);
            Console.ReadKey();
        }
    }
}
